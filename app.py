from flask import Flask, render_template, request, url_for, redirect
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = '127.0.0.1'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Bogota2021'
app.config['MYSQL_DATABASE'] = 'flask_journal'
app.config['MYSQL_UNIX_SOCKET'] = 'TCP'
mysql = MySQL(app)


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/create', methods=['POST'])
def create():
    if request.method == 'POST':
        activity = request.form['activity']
        todo = request.form['todo']
        importance = request.form['importance']
        mood = request.form['mood']
        plan = request.form['plan']

        curs = mysql.connection.cursor()
        curs.execute('USE flask_journal; INSERT INTO records (activity, todo, importance, mood, plan) VALUES (%s,%s,%s,%s,%s)',(activity, todo, importance, mood, plan))
        mysql.connect.commit()

    return redirect(url_for('home'))


@app.route('/read')
def read():
    return 'reading all data'


@app.route('/update')
def update():
    return 'Updating a value'


@app.route('/delete')
def delete():
    return 'Delete an entry'


if __name__ == '__main__':
    app.run(port=3000, debug=True)
